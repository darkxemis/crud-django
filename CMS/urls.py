from CMS import views
from django.urls import path

urlpatterns = [
    path('', views.index, name="index"),
    path('add_note', views.add_note, name="add_note"),
    path('delete_note', views.delete_note, name="delete_note"),
    path('update_note', views.update_note, name="update_note"),
]