from django import forms
from CMS.models import Note

#Clase usada para el formilario del modelo Note
class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ["end_date", "note", "attach","task","tag","type", 'user',]