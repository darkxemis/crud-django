from django.shortcuts import render
from CMS.models import *
from CMS.forms import NoteForm
from django.http import HttpResponse
from django.shortcuts import get_list_or_404, get_object_or_404

#Index ventana principal
def index(request):
    form = NoteForm()
    notes = Note.objects.all().order_by('date')
    return render(request, 'CMS/index.html', locals())

#Se añade una nueva nota procediente del formulario
def add_note(request):
    if request.is_ajax():
        Note.save_note(NoteForm(request.POST, request.FILES))

    return HttpResponse("Everything Okay")

#Elimina una nota dado un Id
def delete_note(request):
    if request.is_ajax():
        id_note = request.POST['id_note']
        Note.delete_note(id_note)
        
    return HttpResponse("Everything Okay")

#Método que actualiza el formulario, se le pasa el formulario y el id y procedemos a llamar a método save del model
def update_note(request):
    if request.is_ajax():
        id_note = request.POST['id_note_update']
        instance = get_object_or_404(Note, id=id_note)
        form = NoteForm(request.POST or None, instance=instance)
        Note.save_note(form)
   
    return HttpResponse("Everything Okay")
