# Generated by Django 2.2.9 on 2020-03-04 15:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CMS', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='attach',
            field=models.FileField(blank=True, upload_to='media'),
        ),
        migrations.AlterField(
            model_name='note',
            name='type',
            field=models.CharField(choices=[('Ex1', 'Example 1'), ('Ex2', 'Example 2')], default='EXAMPLE1', max_length=3),
        ),
    ]
