(function($) {

	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie !== '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = cookies[i].trim();
				if (cookie.substring(0, name.length + 1) === (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	}

	$('td > button[name$="update_button"]').click(function() {
		var id_note = $(this).closest("tr").find("input[name=id_note]").val();
		$('#id_note_update').val(id_note);
	});

	$('#form_update').submit(function() {
		var id_note = $('#id_note_update').val();
		var token = getCookie('csrftoken');

		$.ajax({ 
			headers: { "X-CSRFToken": token },
			//data: {'form': $(this).serialize(), 'id_note': id_note}, 
			data: $(this).serialize(),
			type: "POST", 
			url: "update_note",
			success: function(response) { 
				//delete_row.remove();
				console.log(response);
			},
			error : function(message) {
				console.log(message);
			}
		});
	});

	
	$('td > button[name$="delete_button"]').click(function() {
		var id_note = $(this).closest("tr").find("input[name=id_note]").val();
		var token = getCookie('csrftoken');
		var delete_row = $(this).closest('tr');

		$.ajax({ 
			headers: { "X-CSRFToken": token },
			data: {'id_note': id_note}, 
			type: "POST", 
			url: "delete_note",
			success: function(response) { 
				delete_row.remove();
			},
			error : function(message) {
				console.log(message);
			}
		});
	});

	$(document).ready(function() {
		$('#form').submit(function() { 
            $.ajax({ 
                data: $(this).serialize(),
                type: $(this).attr('method'),
                url: "add_note",
                success: function(response) { 
                    console.log("message");  
				},
				error : function(message) {
					console.log("Cstañazio");
					console.log(message);
				}
            });
        });
	});

})(jQuery);
