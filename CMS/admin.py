from django.contrib import admin
from .models import Note, UserNote

class NoteAdmin(admin.ModelAdmin):
    list_display = ("id", "date", "end_date", "note", "attach","task","tag","type", 'user',)
    list_editable = ("note", )
    #inlines = [ProductImageInline, ]

    fieldsets = (
            (u"Note data", {
                'fields': ('end_date', 'note', "attach", "task", ("tag", "type",), 'user',)
            }),
        )

class UserNoteAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "email",)
    list_editable = ("name", 'email', )
    #inlines = [ProductImageInline, ]

    fieldsets = (
            (u"User note data", {
                'fields': ('name', 'email')
            }),
        )
        
admin.site.register(Note, NoteAdmin)
admin.site.register(UserNote, UserNoteAdmin)