from django.db import models
from taggit.managers import TaggableManager

EXAMPLES = (
    ('Ex1','Example 1'),
    ('Ex2','Example 2'),
    )

#He creado la clase user por petición del ejercicio, pero realmente se podria usar el modelo que usa ya django para administracion de usuarios para ahorar una tabla duplicada.
class UserNote(models.Model):
    name = models.CharField(max_length=23, default="", verbose_name='Name')
    email = models.EmailField(max_length=150, default="", blank=True, verbose_name='Email')

    def __str__(self):
        return self.name

#Clase Note
class Note(models.Model):
    date = models.DateField(auto_now=True, verbose_name="Date now")
    end_date = models.DateField(auto_now=False, verbose_name='Date End')
    note = models.TextField(blank=True, verbose_name='Note')
    attach = models.FileField(upload_to='', blank=True)
    task = models.BooleanField(verbose_name='Task')
    tag = TaggableManager()
    type = models.CharField(max_length=3, choices=EXAMPLES, default='EXAMPLE1')

    user = models.ForeignKey(UserNote, on_delete=models.CASCADE) 

    def __str__(self):
        return self.note

    def save_note(form):
        if form.is_valid():
            form.save()

    def delete_note(id_note):
        note = Note.objects.get(id = id_note)
        note.delete()



